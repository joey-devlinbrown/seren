var app = angular.module('seren', ['ngRoute', 'ngMaterial', 'LocalStorageModule', 'ngFileUpload']);

app.config(function($routeProvider, $httpProvider, $mdThemingProvider) {
 	$routeProvider
  		.when("/", {
      		templateUrl : "views/pages/login.html"
  		})
	$routeProvider
	   	.when("/home", {
	    	templateUrl : "views/pages/home.html"
	  	})
  $routeProvider
    	.when("/account", {
    	   templateUrl : "views/pages/account.html"
    	})
	$routeProvider
  		.when("/register", {
      		templateUrl : "views/pages/register.html"
  		})
	$routeProvider
  		.when("/forgot-password", {
      		templateUrl : "views/pages/forgot.html"
  		})
	$routeProvider
	    .when("/logout", {
	      	templateUrl : "views/pages/logout.html"
	    });

	$httpProvider.defaults.useXDomain = true;

  $mdThemingProvider.definePalette('pastelblue', {
    '50': 'eff3f9',
    '100': 'd6e2ef',
    '200': 'bbcfe5',
    '300': 'a0bbdb',
    '400': '8badd3',
    '500': '779ecb',
    '600': '6f96c6',
    '700': '648cbe',
    '800': '5a82b8',
    '900': '4770ac',
    'A100': 'ffffff',
    'A200': 'd1e3ff',
    'A400': '9ec4ff',
    'A700': '85b5ff',
    'contrastDefaultColor': 'light',
    'contrastDarkColors': [
      '50',
      '100',
      '200',
      '300',
      '400',
      '500',
      '600',
      '700',
      'A100',
      'A200',
      'A400',
      'A700'
    ],
    'contrastLightColors': [
      '800',
      '900'
    ]
  })
  .definePalette('pastelgreen', {
    '50': 'effbef',
    '100': 'd6f5d6',
    '200': 'bbeebb',
    '300': 'a0e7a0',
    '400': '8be28b',
    '500': '77dd77',
    '600': '6fd96f',
    '700': '64d464',
    '800': '5acf5a',
    '900': '47c747',
    'A100': 'ffffff',
    'A200': 'ebffeb',
    'A400': 'b8ffb8',
    'A700': '9eff9e',
    'contrastDefaultColor': 'light',
    'contrastDarkColors': [
      '50',
      '100',
      '200',
      '300',
      '400',
      '500',
      '600',
      '700',
      '800',
      '900',
      'A100',
      'A200',
      'A400',
      'A700'
    ],
    'contrastLightColors': []
  })
  .definePalette('pastelred', {
    '50': 'f8e7e4',
    '100': 'edc4bd',
    '200': 'e19d91',
    '300': 'd47664',
    '400': 'cb5843',
    '500': 'c23b22',
    '600': 'bc351e',
    '700': 'b42d19',
    '800': 'ac2614',
    '900': '9f190c',
    'A100': 'ffd0cd',
    'A200': 'ffa09a',
    'A400': 'ff7067',
    'A700': 'ff584d',
    'contrastDefaultColor': 'light',
    'contrastDarkColors': [
      '50',
      '100',
      '200',
      '300',
      'A100',
      'A200',
      'A400',
      'A700'
    ],
    'contrastLightColors': [
      '400',
      '500',
      '600',
      '700',
      '800',
      '900'
    ]
  });

  $mdThemingProvider.theme('default')
    .primaryPalette('pastelblue', {
      'default': '400', // by default use shade 400 from the pink palette for primary intentions
      'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
      'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
      'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
    })
    .accentPalette('pastelgreen', {
      'default': '200', // use shade 200 for default, and keep all other shades the same
      'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
      'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
      'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
    })
    .warnPalette('pastelred', {
      'default': '200', // use shade 200 for default, and keep all other shades the same
      'hue-1': '100', // use shade 100 for the <code>md-hue-1</code> class
      'hue-2': '600', // use shade 600 for the <code>md-hue-2</code> class
      'hue-3': 'A100' // use shade A100 for the <code>md-hue-3</code> class
    });
});

app.run(function($rootScope) {
	$rootScope.$on("$routeChangeStart", function (event, next, current) {
    if (sessionStorage.restorestate == "true") {
        $rootScope.$broadcast('restorestate'); //let everything know we need to restore state
        sessionStorage.restorestate = false;
    }
	});

	//let everthing know that we need to save state now.
	window.onbeforeunload = function (event) {
  	$rootScope.$broadcast('savestate');
	};
});
