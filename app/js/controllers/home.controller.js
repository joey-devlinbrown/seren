app.controller('HomeController', ['$scope', '$http', 'userService', 'yodleeKit', 'configKit', '$mdDialog', function($scope, $http, userService, yodleeKit, configKit, $mdDialog) {
  outcome = configKit.isUserAuthenticated();
  $scope.user = JSON.parse(localStorage.getItem('User'));

  $scope.loadFastlink = function() {
    $http({
      url: configKit.API_URL+'yodleeFastlink/'+$scope.user.user_id,
      method: "GET"
    })
    .then(function(response) {
        //console.log(response.data);
        $scope.fastlinkParams = response.data;
    });
  };

  $scope.loadBankAccounts = function() {
    $scope.uiLoadingBankAccounts = true;
    $scope.uiLoadingTransactions = true;
    $scope.loadTransactions();
    $http({
      url: configKit.API_URL+'yodleeAccounts/'+$scope.user.user_id,
      method: "GET"
    })
    .then(function(response) {
        console.log(response.data.account);
        $scope.financial = response.data.account;
        $scope.uiLoadingBankAccounts = false;
    });
  };

  $scope.loadTransactions = function() {
    $http({
      url: configKit.API_URL+'yodleeTransactions/'+$scope.user.user_id,
      method: "GET"
    })
    .then(function(response) {
        console.log(response)
        $scope.transactions = response.data;
        $scope.uiLoadingTransactions = false;
    });
  };

  $scope.deleteAccount = function(account_id) {
    $http({
      url: configKit.API_URL+'yodleeAccounts/'+$scope.user.user_id+'/'+account_id,
      method: "DELETE"
    })
    .then(function(response) {
      console.log('deleting.....');
      console.log(response.data);
    });
  };

  $scope.loadFastlink();
  $scope.loadBankAccounts();

  $scope.showAdvanced = function(ev) {
    $mdDialog.show({
      controller: 'HomeTransactionsController',
      templateUrl: 'views/templates/dialog/transactions.html',
      parent: angular.element(document.body),
      targetEvent: ev,
      clickOutsideToClose:true,
      locals: {
        items: $scope.transactions
     }
    });
  };
}]);

app.controller('HomeTransactionsController', ['$scope', 'items', function($scope, items) {
  $scope.transactions = items;
}]);

app.controller('SideNavController', ['$scope', '$timeout', '$mdSidenav', function($scope, $timeout, $mdSidenav) {
  $scope.toggleLeft = buildToggler('left');
    $scope.toggleRight = buildToggler('right');

    function buildToggler(componentId) {
      return function() {
        $mdSidenav(componentId).toggle();
      };
    }
}]);
