app.controller('RegisterController', ['$scope', '$http', 'Upload', '$location', '$timeout', 'userService', '$mdToast', function($scope, $http, Upload, $location, $timeout, userService, $mdToast) {
  $scope.form = {
    first_name: '',
    last_name: '',
    email: '',
    password: '',
    confirmemail: '',
    confirmpassword: ''
  };

  $scope.register = function() {
  	console.log('Register');
    var data = {
        first_name: $scope.form.first_name,
        last_name: $scope.form.last_name,
        email: $scope.form.email,
        password: $scope.form.password,
        guid: $scope.check.guid,
        status: $scope.check.status,
        type: $scope.check.identity.classification.type,
    };
    $http({
      url: 'http://api.serentech.co.uk/users',
      method: "POST",
      data: data
    })
    .then(function(response) {
      if (response.data.success){
        userService.model.first_name = response.data.data.first_name;
        userService.model.last_name =response.data.data.last_name;
        userService.model.email = response.data.data.email;
        userService.model.user_id = response.data.data.user_id;
        userService.model.authenticated = true;
        localStorage.setItem("User", JSON.stringify(userService.model));
        $location.path( "/home" );
      }
    });
  };

  $scope.confirmio = function(file, file2){
    if ($scope.checkForm()){
      $scope.uiConfirmMessage = "Securely uploading your files";
      $scope.uiConfirmLoading = true;
      Upload.upload({
        url: 'http://confirm.serentech.co.uk/initial.endpoint.php',
        data: {front_image: file, back_image: file2, email: $scope.form.email}
      }).then(function(response) {
        $scope.initial = response.data;
        console.log(response);
        $scope.checkStatus();
      });
    } else {
      console.log('failed');
    }
  }

  $scope.checkStatus = function(){
    $scope.uiConfirmMessage = "Checking status of confirmation";
    Upload.upload({
      url: 'http://confirm.serentech.co.uk//check.endpoint.php',
      data: {email: $scope.form.email, guid: $scope.initial.guid}
    }).then(function(response) {
      $scope.check = response.data;
      if ($scope.check.status !== 'pass'){
        $timeout($scope.checkStatus(), 5000);
      } else {
        $scope.uiConfirmMessage = false;
        $scope.uiConfirmLoading = false;
        $scope.clear();
        $scope.clear2();
        console.log(response);
      }
    });
  }

  $scope.clear = function(){
    $scope.myFile = false;
  }

  $scope.clear2 = function(){
    $scope.myFile2 = false;
  }

  $scope.checkForm = function(){
    if (!angular.isDefined($scope.form.first_name) || $scope.form.first_name === '' || $scope.form.first_name === null || $scope.form.first_name === false){
      $mdToast.show(
        $mdToast.simple()
        .textContent('You need to fill in a first name')
        .position('bottom right')
        .hideDelay(3000)
      );
      return false;
    }
    if (!angular.isDefined($scope.form.last_name) || $scope.form.last_name === '' || $scope.form.last_name === null || $scope.form.last_name === false){
      $mdToast.show(
        $mdToast.simple()
        .textContent('You need to fill in a last name')
        .position('bottom right')
        .hideDelay(3000)
      );
      return false;
    }
    if (!angular.isDefined($scope.form.email) || $scope.form.email === '' || $scope.form.email === null || $scope.form.email === false){
      $mdToast.show(
        $mdToast.simple()
        .textContent('You need to fill in a email')
        .position('bottom right')
        .hideDelay(3000)
      );
      return false;
    }
    if (!angular.isDefined($scope.form.password) || $scope.form.password === '' || $scope.form.password === null || $scope.form.password === false){
      $mdToast.show(
        $mdToast.simple()
        .textContent('You need to fill in a password')
        .position('bottom right')
        .hideDelay(3000)
      );
      return false;
    }
    if (!angular.isDefined($scope.form.confirmemail) || $scope.form.confirmemail === '' || $scope.form.confirmemail === null || $scope.form.confirmemail === false){
      $mdToast.show(
        $mdToast.simple()
        .textContent('You need to fill in a confirmation of your email')
        .position('bottom right')
        .hideDelay(3000)
      );
      return false;
    }
    if (!angular.isDefined($scope.form.confirmpassword) || $scope.form.confirmpassword === '' || $scope.form.confirmpassword === null || $scope.form.confirmpassword === false){
      $mdToast.show(
        $mdToast.simple()
        .textContent('You need to fill in a confirmation of your password')
        .position('bottom right')
        .hideDelay(3000)
      );
      return false;
    }
    return true;
  }
}]);
