app.controller('AccountController', ['$scope', '$http', 'userService', 'yodleeKit', 'configKit', function($scope, $http, userService, yodleeKit, configKit) {
  outcome = configKit.isUserAuthenticated();
  $scope.user = JSON.parse(localStorage.getItem('User'));
  console.log($scope.user);
  $scope.getAccountDetails = function(){
    $http({
      url: 'http://api.serentech.co.uk/users/'+$scope.user.user_id,
      method: "GET"
    })
    .then(function(response) {
      console.log(response);
      $scope.accounts = response.data;
    });
  }
  $scope.getAccountDetails();
}]);
