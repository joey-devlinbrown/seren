app.controller('LoginController', ['$scope', '$http', 'userService', '$location', '$mdToast', 'configKit', function($scope, $http, userService, $location, $mdToast, configKit) {
  $scope.username = 'joestockwell58@gmail.com';
  $scope.password = 'secret';
  if (localStorage.getItem('uiLogoutMessage')){
    $scope.uiLogoutMessage = localStorage.getItem('uiLogoutMessage');
    localStorage.removeItem('uiLogoutMessage');
  }

  $scope.login = function() {
    var data = {
        email: $scope.username,
        password: $scope.password,
        secret: 'MXjaK85K4RspkFiO3ZThvdnZ1W0XVmty'
    };

    $http({
      url: configKit.API_URL+'auth',
      method: "POST",
      data: data
    })
    .then(function(response) {
      if (response.data.success){
        userService.model.user_id = response.data.data.user_id;
        userService.model.first_name = response.data.data.first_name;
        userService.model.last_name =response.data.data.last_name;
        userService.model.email = response.data.data.email;
        userService.model.authenticated = true;
        localStorage.setItem("User", JSON.stringify(userService.model));
        $location.path( "/home" );
      } else {
        $mdToast.show(
          $mdToast.simple()
          .textContent('There was a problem with the credentials you provided.')
          .position('bottom right')
          .hideDelay(5000)
        );
      };
    });
  };

  function setServiceData(){
    localStorageService.set('userService',userService.model);
  }

  $scope.register = function() {
  	console.log('To the register page');
  };
  $scope.forgotPassword = function() {
  	console.log('To the forgot password page');
  };
}]);
