app.controller('LogoutController', ['$scope', '$http', 'userService', '$location', function($scope, $http, userService, $location) {
  user = JSON.parse(localStorage.getItem('User'));
  email = user.email
  localStorage.clear();
  localStorage.setItem('uiLogoutMessage', email+' was logged out.');
  $location.path( "/" );
}]);
