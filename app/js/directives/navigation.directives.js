app.directive('ngNav', function() {
  return {
	templateUrl: 'views/templates/navigation.html'
  }
});

app.directive('ngNavRestrict', function() {
  return {
	templateUrl: 'views/templates/navigation-restrict.html'
  }
});