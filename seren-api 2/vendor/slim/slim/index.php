<?php
require 'Slim/Slim.php';
require '../../autoload.php';
//require '../../diversen/redbean-composer/rb.php';
//require '../../tuupola/slim-jwt-auth/src/JwtAuthentication.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();
class ResourceNotFoundException extends Exception {}
R::setup('mysql:host=localhost;dbname=seren', 'root', '33B53*$Dq9cs');
R::freeze(true);

$app->get(
    '/',
    function () {
        echo 'This is the Seren API';
    }
);
$app->post(
    '/post',
    function () {
        echo 'This is a POST route';
    }
);
$app->put(
    '/put',
    function () {
        echo 'This is a PUT route';
    }
);
$app->patch('/patch', function () {
    echo 'This is a PATCH route';
});
$app->delete(
    '/delete',
    function () {
        echo 'This is a DELETE route';
    }
);


$app->get('/articles', function () use ($app) {
  $articles = R::find('articles');
  $app->response()->header('Content-Type', 'application/json');
  echo json_encode(R::exportAll($articles));
});

$app->get('/articles/:id', function ($id) use ($app) {
  try {
    $article = R::findOne('articles', 'id=?', array($id));

    if ($article) {
      $app->response()->header('Content-Type', 'application/json');
      echo json_encode(R::exportAll($article));
    } else {
      throw new ResourceNotFoundException();
    }
  } catch (ResourceNotFoundException $e) {
    $app->response()->status(404);
  } catch (Exception $e) {
    $app->response()->status(400);
    $app->response()->header('X-Status-Reason', $e->getMessage());
  }
});

$app->post('/articles', function () use ($app) {
  try {
    $input = $app->request->post();

    $article = R::dispense('articles');
    $article->title = $input['title'];
    $article->url = $input['url'];
    $article->date = $input['date'];
    $id = R::store($article);

    $app->response()->header('Content-Type', 'application/json');
    echo json_encode(R::exportAll($article));
  } catch (Exception $e) {
    $app->response()->status(400);
    $app->response()->header('X-Status-Reason', $e->getMessage());
  }
});

$app->get('/users', function () use ($app) {
  $users = R::find('users_view');
  $app->response()->header('Content-Type', 'application/json');
  echo json_encode(R::exportAll($users));
});

$app->get('/users/:id', function ($id) use ($app) {
  try {
    $user = R::findOne('users_view', 'id=?', array($id));

    if ($user) {
      $app->response()->header('Content-Type', 'application/json');
      echo json_encode(R::exportAll($user));
    } else {
      throw new ResourceNotFoundException();
    }
  } catch (ResourceNotFoundException $e) {
    $app->response()->status(404);
  } catch (Exception $e) {
    $app->response()->status(400);
    $app->response()->header('X-Status-Reason', $e->getMessage());
  }
});

$app->post('/users', function () use ($app) {
  try {
    $input = $app->request->post();

    $user = R::dispense('users');
    $user->first_name = $input['first_name'];
    $user->last_name = $input['last_name'];
    $user->email = $input['email'];
    $user->hash = password_hash($input['password'], PASSWORD_DEFAULT);

    $id = R::store($user);

    $app->response()->header('Content-Type', 'application/json');
    unset($user['hash']);
    echo json_encode(R::exportAll($user));
  } catch (Exception $e) {
    $app->response()->status(400);
    $app->response()->header('X-Status-Reason', $e->getMessage());
  }
});

$app->post('/auth', function () use ($app) {
  try {
    $input = $app->request->post();
    $approved_apps = R::getAll( 'SELECT * FROM apps WHERE active = 1 AND secret = :secret',
      [':secret' => $input['secret']]
    );

    if (count($approved_apps) === 1) { //should be unique
      $user = R::findOne('users', 'email=?', array($input['email']));
      $user = R::exportAll($user);
      $auth = password_verify($input['password'], $user[0]['hash']);

      if ($user) {
        $app->response()->header('Content-Type', 'application/json');
        echo json_encode($auth);
      } else {
        throw new ResourceNotFoundException();
      }
    }
    else {
      throw new ResourceNotFoundException();
    }
  } catch (ResourceNotFoundException $e) {
    $app->response()->status(404);
  } catch (Exception $e) {
    $app->response()->status(400);
    $app->response()->header('X-Status-Reason', $e->getMessage());
  }
});

$app->run();
