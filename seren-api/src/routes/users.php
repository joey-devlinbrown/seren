<?php

$app->post('/users', function ($req, $res, $args) {
  try {
    $input = $req->getParsedBody();

    $user = R::dispense('users');
    $user->first_name = $input['first_name'];
    $user->last_name = $input['last_name'];
    $user->email = $input['email'];
    $user->hash = password_hash($input['password'], PASSWORD_DEFAULT);
    $id = R::store($user);
    $user->id = $id;
    $user->user_id = $id;

    $confirm = R::dispense('confirm');
    $confirm->user_id = $id;
    $confirm->guid = $input['guid'];
    $confirm->status = $input['status'];
    $confirm->type = $input['type'];
    R::store($confirm);
    unset($user['hash']);
    $data = [
      'success' => true,
      'message' => 'Logged in successfully',
      'data' => $user
    ];
    return $res->withJson($data);
  } catch (ResourceNotFoundException $e) {
    return $res->withStatus(404);
  } catch (Exception $e) {
    return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
  }
});

$app->get(
    '/users/{id}',
    function ($req, $res, $args) {
      try {
        $id = $req->getAttribute('id');
        $user = R::findOne('users', 'id=?', array($id));
        if ($user) {
          $return['user'] = R::exportAll($user);
          $yodlee = R::findOne('yodleelinks', 'user_id=?', array($id));
          if ($yodlee){
            $return['yodlee'] = R::exportAll($yodlee);
          }
          $confirm = R::findOne('confirm', 'user_id=?', array($id));
          if ($confirm){
            $return['confirm'] = R::exportAll($confirm);
          }
          return $res->withJson($return);
        } else {
          return $res->withJson(false);
        }
      } catch (ResourceNotFoundException $e) {
        return $res->withStatus(404);
      } catch (Exception $e) {
        return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
      }
    }
);
