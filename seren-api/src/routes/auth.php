<?php

$app->post('/auth', function ($req, $res, $args) {
  try {
    $input = $req->getParsedBody();
    $approved_apps = R::getAll( 'SELECT * FROM apps WHERE active = 1 AND secret = :secret',
      [':secret' => $input['secret']]
    );

    if (count($approved_apps) === 1) { //should be unique
      $user = R::findOne('users', 'email=?', array($input['email']));
      $user = R::exportAll($user);
      $auth = password_verify($input['password'], $user[0]['hash']);

      if ($auth) {
        //$token = JWT::encode($auth, "thisissecret", "HS256");
        unset($user[0]['hash']);
        $user[0]['user_id'] = $user[0]['id'];
        unset($user[0]['id']);
        $data = [
          'success' => true,
          'message' => 'Logged in successfully',
          'data' => $user[0]
        ];
        return $res->withJson($data);
      } else {
        $data = [
          'success' => false,
          'message' => 'There was a problem with the credentials you provided.'
        ];
        return $res->withJson($data);
      }
    }
    else {
      $data = [
        'success' => false,
        'message' => 'There was a problem with the credentials you provided.'
      ];
      return $res->withJson($data);
    }
  } catch (ResourceNotFoundException $e) {
    return $res->withStatus(404);
  } catch (Exception $e) {
    return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
  }
});
