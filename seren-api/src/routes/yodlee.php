<?php

$app->get(
    '/yodlee/{user_id}',
    function ($req, $res, $args) {
      try {
        $user_id = $req->getAttribute('user_id');
        $yodleelink = R::findOne('yodleelinks', 'user_id=?', array($user_id));

        if ($yodleelink) {
          $yodleelink = R::exportAll($yodleelink);
          $CUSTOM_ENC = new Enc();
          $yodleelink[0]['password'] = $CUSTOM_ENC->decrypt($yodleelink[0]['password']);
          return $res->withJson($yodleelink[0]);
          //json_encode(R::exportAll($article));
        } else {
          return $res->withJson(false);
        }
      } catch (ResourceNotFoundException $e) {
        return $res->withStatus(404);
      } catch (Exception $e) {
        return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
      }
    }
);

$app->post(
    '/yodlee',
    function ($req, $res, $args) {
      try {
        $input = $req->getParsedBody();
        $yodleeLink = R::dispense('yodleelinks');
        $yodleeLink->username = $input['username'];
        $yodleeLink->password = $CUSTOM_ENC->encrypt($input['password']);
        $yodleeLink->user_id = $input['user_id'];
        $id = R::store($yodleeLink);
        echo json_encode(R::exportAll($yodleeLink));
      } catch (ResourceNotFoundException $e) {
        return $res->withStatus(404);
      } catch (Exception $e) {
        return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
      }
    }
);

$app->delete(
  '/yodlee/{id}',
  function ($req, $res, $args) {
    try {
      $id = $req->getAttribute('id');
      $yodleelink = R::findOne('yodleelinks', 'user_id=?', array($id));

      if ($yodleelink) {
        R::trash($yodleelink);
      } else {
        return $res->withStatus(404);
      }
    } catch (ResourceNotFoundException $e) {
      return $res->withStatus(404);
    } catch (Exception $e) {
      return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
    }
  }
);

$app->get(
    '/yodleeAccounts/{user_id}',
    function ($req, $res, $args) {
      try {
        $user_id = $req->getAttribute('user_id');
        $CUSTOM_Yodlee = new Yodlee();
        $result = $CUSTOM_Yodlee->getAccounts($user_id);
        //return $res->withJson($result['accountd']);
      } catch (ResourceNotFoundException $e) {
        return $res->withStatus(404);
      } catch (Exception $e) {
        return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
      }
    }
);

$app->delete(
  '/yodleeAccounts/{user_id}/{account_id}',
  function ($req, $res, $args) {
    try {
      $user_id = $req->getAttribute('user_id');
      $account_id = $req->getAttribute('account_id');
      $CUSTOM_Yodlee = new Yodlee();
      $result = $CUSTOM_Yodlee->deleteAccount($user_id, $account_id);
      //echo $user_id;
      //echo $account_id;
    } catch (ResourceNotFoundException $e) {
      return $res->withStatus(404);
    } catch (Exception $e) {
      return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
    }
  }
);

$app->get(
    '/yodleeTransactions/{user_id}',
    function ($req, $res, $args) {
      try {
        $user_id = $req->getAttribute('user_id');
        $CUSTOM_Yodlee = new Yodlee();
        $result = $CUSTOM_Yodlee->getTransactions($user_id);
        //echo json_encode($result);
      } catch (ResourceNotFoundException $e) {
        return $res->withStatus(404);
      } catch (Exception $e) {
        return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
      }
    }
);

$app->get(
    '/yodleeFastlink/{user_id}',
    function ($req, $res, $args) {
      try {
        $user_id = $req->getAttribute('user_id');
        $CUSTOM_Yodlee = new Yodlee();
        $result = $CUSTOM_Yodlee->getFastLinkPayload($user_id);
        return $res->withJson($result);
      } catch (ResourceNotFoundException $e) {
        return $res->withStatus(404);
      } catch (Exception $e) {
        return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
      }
    }
);
