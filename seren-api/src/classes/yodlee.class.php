<?php
class Yodlee
{
    private $API_URL = 'https://developer.api.yodlee.com/ysl/restserver/v1/';
    private $cobrandLogin = 'sbCobjsdevlinbrown';
    private $cobrandPassword = 'eec1763c-c75c-40a4-ad87-234543ddfbd3';
    private $locale = 'en_US';

    public function getAccounts($user_id)
    {
        $cobSession  = $this->getCobToken();
        $userSession = $this->getUserToken($user_id);
        $headers     = array(
            'Authorization: cobSession=' . $cobSession . ',userSession=' . $userSession
        );
        $curl        = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->API_URL . 'accounts');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($curl);
        if ($result === FALSE) {
            curl_close($curl);
            return $result;
        } else {
            curl_close($curl);
            return $result;
        }
    }

    public function deleteAccount($user_id, $account_id)
    {
        $cobSession  = $this->getCobToken();
        $userSession = $this->getUserToken($user_id);
        $headers     = array(
            'Authorization: cobSession=' . $cobSession . ',userSession=' . $userSession
        );
        $curl        = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->API_URL . 'accounts/' . $account_id);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "DELETE");
        $result = curl_exec($curl);
        if ($result === FALSE) {
            curl_close($curl);
            return $result;
        } else {
            curl_close($curl);
            return $result;
        }
    }

    public function getTransactions($user_id)
    {
        $cobSession  = $this->getCobToken();
        $userSession = $this->getUserToken($user_id);
        $headers     = array(
            'Authorization: cobSession=' . $cobSession . ',userSession=' . $userSession
        );
        $curl        = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->API_URL . 'transactions');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        $result = curl_exec($curl);
        if ($result === FALSE) {
            curl_close($curl);
            return $result;
        } else {
            curl_close($curl);
            return $result;
        }
    }

    public function getFastLinkPayload($user_id)
    {
        $data['app'] = '10003600';
        $data['rsession'] = $this->getUserToken($user_id);
        $data['token'] = $this->getFastLinkToken($user_id);
        $data['redirectReq'] = true;
        $data['extraParams'] = 'callback=http://localhost:8888/seren/app/#!/home';
        return $data;
    }

    private function getFastLinkToken($user_id){
        $cobSession  = $this->getCobToken();
        $userSession = $this->getUserToken($user_id);
        $headers     = array(
            'Authorization: cobSession=' . $cobSession . ',userSession=' . $userSession
        );
        $curl        = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->API_URL . 'user/accessTokens?appIds=10003600');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl);
        if ($result === FALSE) {
            curl_close($curl);
            return $result;
        } else {
            curl_close($curl);
            $result = json_decode($result);
            $result = $result->user;
            $result = $result->accessTokens;
            $result = $result[0]->value;
            return $result;
        }
    }

    private function getCobToken() //find out if a cobrand token currently exists and is less than 55 minutes old
    {
        $rows = R::getAll('SELECT * FROM yodleecobtokens WHERE NOW() > (created_at - INTERVAL 55 MINUTE)');
        if (count($rows) < 1) {
            return $this->generateCobToken();
        } else {
            return $rows[0]['token'];
        }
    }

    private function getUserToken($user_id) //find out if a cobrand token currently exists and is less than 25 minutes old
    {
        $rows = R::getAll('SELECT * FROM yodleeusertokens WHERE NOW() > (created_at - INTERVAL 25 MINUTE) AND user_id = :user_id', [':user_id' => $user_id]);
        if (count($rows) < 1) {
            return $this->generateUserToken($user_id);
        } else {
            return $rows[0]['token'];
        }
    }

    private function generateCobToken(){
        $headers = array();
        $headers[] = 'Content-Type: text/plain';

        $data = array();
        $data['cobrandLogin'] = $this->cobrandLogin;
        $data['cobrandPassword'] = $this->cobrandPassword;
        $data2 = array();
        $data2['cobrand'] = $data;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->API_URL.'cobrand/login',
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => json_encode($data2)
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        $sessionToken = json_decode($resp);
        $sessionToken = $sessionToken->session;
        $sessionToken = $sessionToken->cobSession;
        $this->storeCobToken($sessionToken);
        return $sessionToken;
    }

    private function generateUserToken($user_id){
        $cobSession = $this->getCobToken();
        $loginParams = $this->getYodleeDetails($user_id);
        $headers = array(
          'Content-Type: text/plain',
          'Authorization: cobSession='.$cobSession
        );

        $data = array();
        $data['user'] = $loginParams;

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $this->API_URL.'user/login',
            CURLOPT_POST => 1,
            CURLOPT_HTTPHEADER => $headers,
            CURLOPT_POSTFIELDS => json_encode($data)
        ));
        $resp = curl_exec($curl);
        curl_close($curl);
        $sessionToken = json_decode($resp);
        $sessionToken = $sessionToken->user;
        $sessionToken = $sessionToken->session;
        $sessionToken = $sessionToken->userSession;
        $this->storeUserToken($sessionToken, $user_id);
        return $sessionToken;
    }

    private function storeCobToken($token){ //store the cobrantToken
        $this->removeAllCobTokens(); //remove all current tokens stored in the database -- here as the setup may be more complex one day
        $cobToken = R::dispense('yodleecobtokens');
        $cobToken->token = $token;
        $id = R::store($cobToken); //store token
        return $id;
    }

    private function storeUserToken($token, $user_id){ //store the cobrantToken
        $this->removeUsersUserTokens($user_id); //remove all current tokens stored in the database -- here as the setup may be more complex one day
        $userToken = R::dispense('yodleeusertokens');
        $userToken->token = $token;
        $userToken->user_id = $user_id;
        $id = R::store($userToken); //store token
        return $id;
    }

    private function getYodleeDetails($user_id)
    {
        $rows                 = R::getAll('SELECT * FROM yodleelinks WHERE user_id = :user_id', [':user_id' => $user_id]);
        $CUSTOM_ENC           = new Enc();
        $details['loginName'] = $CUSTOM_ENC->decrypt($rows[0]['username']);
        $details['password']  = $CUSTOM_ENC->decrypt($rows[0]['password']);
        return $details;
    }

    private function removeAllCobTokens()
    {
        R::exec('DELETE FROM yodleecobtokens');
    }

    private function removeUsersUserTokens($user_id){
        R::exec('DELETE FROM yodleeusertokens WHERE user_id = :user_id', [':user_id' => $user_id]);
    }
}
