<?php

require __DIR__ . '/routes/auth.php';
require __DIR__ . '/routes/users.php';
require __DIR__ . '/routes/yodlee.php';

$app->get(
    '/playground/{user_id}',
    function ($req, $res, $args) {
      $user_id = intval($req->getAttribute('user_id'));
      $CUSTOM_Yodlee = new Yodlee();
      $result = $CUSTOM_Yodlee->getFastLinkToken($user_id);
      //$results['cob'] = $CUSTOM_Yodlee->getCobToken();
      //$results['user'] = $CUSTOM_Yodlee->getUserToken($user_id);
      //$CUSTOM_ENC = new Enc();
      //$results['username'] = $CUSTOM_ENC->encrypt('sbMemjsdevlinbrown1');
      //$results['password'] = $CUSTOM_ENC->encrypt('sbMemjsdevlinbrown1#123');
      return $res->withJson($result);
    }
);

/*
$app->get('/articles', function ($req, $res, $args) {
  try {
    $articles = R::find('articles');
    echo json_encode(R::exportAll($articles));
  } catch (ResourceNotFoundException $e) {
    return $res->withStatus(404);
  } catch (Exception $e) {
    return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
  }
});

$app->get('/articles/{id}', function ($req, $res, $args) {
  try {
    $id = $req->getAttribute('id');
    $article = R::findOne('articles', 'id=?', array($id));

    if ($article) {
      $article = R::exportAll($article);
      return $res->withJson($article);
      //json_encode(R::exportAll($article));
    } else {
      return $res->withStatus(404);
    }
  } catch (ResourceNotFoundException $e) {
    return $res->withStatus(404);
  } catch (Exception $e) {
    return $res->withStatus(400)->withHeader('X-Status-Reason', $e->getMessage());
  }
});

$app->post('/articles', function () {
  try {
    $input = $app->request->post();

    $article = R::dispense('articles');
    $article->title = $input['title'];
    $article->url = $input['url'];
    $article->date = $input['date'];
    $id = R::store($article);

    $app->response()->header('Content-Type', 'application/json');
    echo json_encode(R::exportAll($article));
  } catch (Exception $e) {
    $app->response()->status(400);
    $app->response()->header('X-Status-Reason', $e->getMessage());
  }
});

$app->get('/users', function () {
  $users = R::find('users_view');
  $app->response()->header('Content-Type', 'application/json');
  echo json_encode(R::exportAll($users));
});

$app->get('/users/:id', function ($id) {
  try {
    $user = R::findOne('users_view', 'id=?', array($id));

    if ($user) {
      $app->response()->header('Content-Type', 'application/json');
      echo json_encode(R::exportAll($user));
    } else {
      throw new ResourceNotFoundException();
    }
  } catch (ResourceNotFoundException $e) {
    $app->response()->status(404);
  } catch (Exception $e) {
    $app->response()->status(400);
    $app->response()->header('X-Status-Reason', $e->getMessage());
  }
});

$app->post('/users', function () {
  try {
    $input = $app->request->post();

    $user = R::dispense('users');
    $user->first_name = $input['first_name'];
    $user->last_name = $input['last_name'];
    $user->email = $input['email'];
    $user->hash = password_hash($input['password'], PASSWORD_DEFAULT);

    $id = R::store($user);

    $app->response()->header('Content-Type', 'application/json');
    unset($user['hash']);
    echo json_encode(R::exportAll($user));
  } catch (Exception $e) {
    $app->response()->status(400);
    $app->response()->header('X-Status-Reason', $e->getMessage());
  }
});
*/
