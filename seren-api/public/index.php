<?php
require __DIR__ . '/../src/cors/header.php';

date_default_timezone_set("Europe/London");

if (PHP_SAPI == 'cli-server') {
    // To help the built-in PHP dev server, check if the request was actually for
    // something which should probably be served as a static file
    $url  = parse_url($_SERVER['REQUEST_URI']);
    $file = __DIR__ . $url['path'];
    if (is_file($file)) {
        return false;
    }
}

//REQUIRES
require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/diversen/redbean-composer/rb.php';

session_start();

// Instantiate the app
$settings = require __DIR__ . '/../src/settings.php';
$app = new \Slim\App($settings);

//R::setup('mysql:host=localhost;dbname=seren-dev', 'root', '33B53*$Dq9cs');
R::setup('mysql:host=localhost;dbname=seren-dev', 'root', 'root');
R::freeze(true);
//R::setup();

//include classes for PHP
require __DIR__ . '/../src/classes/include.php';

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Register middleware
require __DIR__ . '/../src/middleware.php';

// Register routes (include partials in order first, and assume routes.php has misc routes)
require __DIR__ . '/../src/routes.php';

// Run app
$app->run();
