<?php

header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST');
header("Access-Control-Allow-Headers: X-Requested-With");

//ini_set('display_errors', 0);
//ini_set('display_startup_errors', 0);

$file1 = false;
$file2 = false;

include_once('../classes/confirm.class.php');

$confirmIo = new confirmIo();
//echo json_encode($_POST);

if (!file_exists("../uploads/{$_POST['email']}"))
  mkdir("../uploads/{$_POST['email']}", 0777, true);

foreach ($_FILES as $FILE => $VALUE){
  $result = $confirmIo->upload($FILE, $VALUE, $_POST['email']);
  if ($file1 === false){
    $file1 = $result;
  } elseif ($file2 === false){
    $file2 = $result;
  }
}

$result = $confirmIo->getInitialGuid($_POST['email'], $file1, $file2);
echo $result;
