<?php

class confirmIo {

  private $token = 'ZmUxNGIyZTEtYzM1NC00M2FmLWE0OTAtNTRmNTU1MmQ1YjNmOg==';

  public function upload($FILE, $VALUE, $email){
    if ($FILE == 'front_image' || $FILE == 'back_image'){
      $identityName = $FILE . '.' . pathinfo($VALUE['name'],PATHINFO_EXTENSION);
      $tempPath = $VALUE[ 'tmp_name' ];
      $uploadPath = '../uploads/'.$email.'/'.$identityName;
      unlink(realpath($uploadPath));
      $outcome = move_uploaded_file( $tempPath, $uploadPath );
      return $identityName;
    }
  }

  public function getInitialGuid($email, $file1, $file2){
    //$debug = true; //use this to log out some more stuff
    $api_url = 'https://api.confirm.io/v1/ids';
    $front_image_location = realpath('../uploads/'.$email.'/'.$file1);
    $back_image_location = realpath('../uploads/'.$email.'/'.$file2);

    $front_image = '@'.$front_image_location;
    $back_image = '@'.$back_image_location;

    /* POPULATE POST ARRAY */
    $postfields = array();
    $postfields['frontImage'] = new CURLFile($front_image_location);
    $postfields['backImage'] = new CURLFile($back_image_location);

    /* GET FILESIZE FOR CURL */
    $filesize = filesize($front_image_location) + filesize($back_image_location);

    /* SET HEADERS */
    $headers = array(
      'Content-Type: multipart/form-data',
      'authorization: Basic '.$this->token
    );

    /* INITIATE CURL */
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $api_url);
    curl_setopt($curl, CURLOPT_POST, true); //setting of -X POST
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
    curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
    curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 100);

    $result = curl_exec ($curl);

    unlink(realpath('../uploads/'.$email.'/'.$file1));
    unlink(realpath('../uploads/'.$email.'/'.$file2));

    if ($result === FALSE) {
      curl_close ($curl);
      return false;
    }else{
      curl_close ($curl);
      return $result;
    }
  }

  public function checkStatus($guid, $email){
    $api_url = 'https://api.confirm.io/v1/ids/'.$guid;
    $headers = array(
        'Content-Type: multipart/form-data',
        'authorization: Basic ZmUxNGIyZTEtYzM1NC00M2FmLWE0OTAtNTRmNTU1MmQ1YjNmOg=='
    );

    /* INITIATE CURL */
    $curl = curl_init();

    curl_setopt($curl, CURLOPT_URL, $api_url);
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible;)");
    curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
    curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 100);

    $result = curl_exec ($curl);
    return $result;
  }
}

?>
